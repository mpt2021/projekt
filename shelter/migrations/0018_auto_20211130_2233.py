# Generated by Django 3.2.9 on 2021-11-30 21:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shelter', '0017_product_waga'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product',
            old_name='waga',
            new_name='weight',
        ),
        migrations.AddField(
            model_name='product',
            name='size',
            field=models.CharField(default='Brak', max_length=100),
        ),
        migrations.AddField(
            model_name='product',
            name='special_signs',
            field=models.CharField(default='Brak', max_length=100),
        ),
    ]
